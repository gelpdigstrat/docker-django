# docker-django

For django projects

## Configuration options

Use the following environment variables

* **UWSGI_PROCESSES** - number of uwsgi worker processes. See http://uwsgi-docs.readthedocs.io/en/latest/Options.html#processes

## How to use this image

```
docker run --name myapp -d -v /my/app:/home/django/app telusgelp/django:3.6-uwsgi
```

## UWSGI

uwsgi is set to listen on port 8080
